# java-app-docker-demo
This demo shows two steps.

+ Install `docker-ce` on Amazon-linux
+ Build and run a simple docker image with a java+nginx-proxy web application.

## Install docker-ce on Amazon Linux
Refer to https://docs.aws.amazon.com/AmazonECS/latest/developerguide/docker-basics.html
You can also find [other OS installation docs from here](https://docs.docker.com/engine/installation).


Other commands:

+ check docker status 
```
sudo systemctl status docker.service
```

+ stop docker 
```
sudo systemctl stop docker
```

+ uninstall docker-ce
```
sudo yum remove docker-ce
```

+ remove all images, container, volumes
```
sudo rm -rf /var/lib/docker
```

## Build/Run a simple java+nginx-proxy docker web app 

#### Clone repo on EC2 Instance in /opt folder

```
cd /opt
sudo git clone https://hmohsin@bitbucket.org/hmohsin/java-application.git
```

#### Build Docker image

```
cd java-application
sudo docker build -t java-application:latest .
cd proxy
sudo docker build -t proxy:latest .
```

#### Run your application using docker compose file
```
cd java-application
sudo docker-compose up -d
```


You can use `sudo docker ps` to list all running containers. 
```
$ sudo docker ps
CONTAINER ID        IMAGE                 COMMAND                  CREATED             STATUS              PORTS                    NAMES
f4ee80e71e48        proxy:latest                "apache2ctl -D FOREG…"   3 days ago          Up 3 days           0.0.0.0:80->80/tcp       proxy
7a894fd8ea82        app:latest                  "java -jar /app/hell…"   3 days ago          Up 3 days           0.0.0.0:8080->8080/tcp   app
```

`52e5f8c631aa` is the running container id. Some commands below are what you might need.

+ display logs in running container
```
    $ sudo docker logs 7a894fd8ea82

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v1.4.2.RELEASE)

2020-05-18 07:36:08.957  INFO 1 --- [           main] com.mkyong.SpringBootWebApplication      : Starting SpringBootWebApplication v1.0 on 7a894fd8ea82 with PID 1 (/app/helloworld.war started by root in /)
2020-05-18 07:36:08.969  INFO 1 --- [           main] com.mkyong.SpringBootWebApplication      : No active profile set, falling back to default profiles: default
2020-05-18 07:36:09.176  INFO 1 --- [           main] ationConfigEmbeddedWebApplicationContext : Refreshing org.springframework.boot.context.embedded.AnnotationConfigEmbeddedWebApplicationContext@14514713: startup date [Mon May 18 07:36:09 GMT 2020]; root of context hierarchy
2020-05-18 07:36:12.813  INFO 1 --- [           main] s.b.c.e.t.TomcatEmbeddedServletContainer : Tomcat initialized with port(s): 8080 (http)
2020-05-18 07:36:12.860  INFO 1 --- [           main] o.apache.catalina.core.StandardService   : Starting service Tomcat
2020-05-18 07:36:12.862  INFO 1 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet Engine: Apache Tomcat/8.5.6
2020-05-18 07:36:15.515  INFO 1 --- [ost-startStop-1] org.apache.jasper.servlet.TldScanner     : At least one JAR was scanned for TLDs yet contained no TLDs. Enable debug logging for this logger for a complete list of JARs that were scanned but no TLDs were found in them. Skipping unneeded JARs during scanning can improve startup time and JSP compilation time.
2020-05-18 07:36:16.259  INFO 1 --- [ost-startStop-1] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2020-05-18 07:36:16.259  INFO 1 --- [ost-startStop-1] o.s.web.context.ContextLoader            : Root WebApplicationContext: initialization completed in 7094 ms
2020-05-18 07:36:16.577  INFO 1 --- [ost-startStop-1] o.s.b.w.servlet.ServletRegistrationBean  : Mapping servlet: 'dispatcherServlet' to [/]
2020-05-18 07:36:16.589  INFO 1 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'characterEncodingFilter' to: [/*]
2020-05-18 07:36:16.590  INFO 1 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'hiddenHttpMethodFilter' to: [/*]
2020-05-18 07:36:16.590  INFO 1 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'httpPutFormContentFilter' to: [/*]
2020-05-18 07:36:16.591  INFO 1 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'requestContextFilter' to: [/*]
2020-05-18 07:36:17.271  INFO 1 --- [           main] s.w.s.m.m.a.RequestMappingHandlerAdapter : Looking for @ControllerAdvice: org.springframework.boot.context.embedded.AnnotationConfigEmbeddedWebApplicationContext@14514713: startup date [Mon May 18 07:36:09 GMT 2020]; root of context hierarchy
2020-05-18 07:36:17.433  INFO 1 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/]}" onto public java.lang.String com.mkyong.WelcomeController.welcome(java.util.Map<java.lang.String, java.lang.Object>)
2020-05-18 07:36:17.448  INFO 1 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/error],produces=[text/html]}" onto public org.springframework.web.servlet.ModelAndView org.springframework.boot.autoconfigure.web.BasicErrorController.errorHtml(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)
2020-05-18 07:36:17.449  INFO 1 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/error]}" onto public org.springframework.http.ResponseEntity<java.util.Map<java.lang.String, java.lang.Object>> org.springframework.boot.autoconfigure.web.BasicErrorController.error(javax.servlet.http.HttpServletRequest)
2020-05-18 07:36:17.525  INFO 1 --- [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/webjars/**] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
2020-05-18 07:36:17.530  INFO 1 --- [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/**] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
2020-05-18 07:36:17.613  INFO 1 --- [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/**/favicon.ico] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
2020-05-18 07:36:18.471  INFO 1 --- [           main] o.s.j.e.a.AnnotationMBeanExporter        : Registering beans for JMX exposure on startup
2020-05-18 07:36:18.610  INFO 1 --- [           main] s.b.c.e.t.TomcatEmbeddedServletContainer : Tomcat started on port(s): 8080 (http)
```

+ stop your container
    ```
    $ sudo docker-compose down
    ```

#### Test your application
```
$ curl http://localhost:80
<!DOCTYPE html>


<html lang="en">
<head>

<link rel="stylesheet" type="text/css"
        href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />

<!--

        <link href="/css/main.css;jsessionid=F238D366D5E9B293FBAA67B5A8B3B778" rel="stylesheet" />
         -->

<link href="/css/main.css;jsessionid=F238D366D5E9B293FBAA67B5A8B3B778" rel="stylesheet" />

</head>
<body>
```